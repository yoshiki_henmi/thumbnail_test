package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	"bitbucket.org/yoshiki_henmi/thumbnail_test/controller"
)

func main() {
	// Echoのインスタンス作る
	e := echo.New()

	// 全てのリクエストで差し込みたいミドルウェア（ログとか）はここ
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// ルーティング
	// e.Get("/hello", controller.MainPage())

	// // サーバー起動
	// e.Run(standard.New(":1323")) //ポート番号指定してね

	controller.MainPage()
}
